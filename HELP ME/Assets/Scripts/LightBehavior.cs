﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBehavior : MonoBehaviour
{
    Transform playerTransform;
    // Start is called before the first frame update
    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        lineOfSight();
    }

    void lineOfSight()
    {
        Vector3 directionToPlayer = playerTransform.position - transform.position;
        directionToPlayer = Vector3.Normalize(directionToPlayer);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, directionToPlayer, out hit) && hit.transform.tag == "Player")
        {
            //print("Seen");
            //playerHasBeenDetected = true;
            // print("You are Visible");
        }
        else if (Physics.Raycast(transform.position, directionToPlayer, out hit) && hit.transform.tag != "Player")
        {
           // print("Not Seen");
            //playerHasBeenDetected = false;
            //print("You are not Visible");
        }

        

        Debug.DrawRay(transform.position, directionToPlayer * 1000, Color.red);
        Debug.DrawRay(transform.position, directionToPlayer * 5, Color.blue);

    }
}
