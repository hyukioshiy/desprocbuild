﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycast : MonoBehaviour
{
    public bool isLaying;
    GameObject water;
    public GameObject eggs;

    bool isDelaying;
    float dist;
    
    public Vector3 offset = new Vector3(0,0,-1);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RayCast();
        if(isLaying)
        {   
            this.GetComponent<Movement>().canMove = false;
            dist = Vector3.Distance(this.transform.position,water.transform.position);
            if(dist > .7f)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position,water.transform.position,.2f);
            }
            else
            {
                GetComponent<Rigidbody>().velocity = Vector3.zero;
                StartCoroutine(Laying());
                //LayEggs();
            }
        }
    }

    void RayCast()
    {
        
        Vector3 rayOrigin = new Vector3(0.5f, 0.5f, 0f); // center of the screen
        float rayLength = 20f;

        Ray ray = Camera.main.ViewportPointToRay(rayOrigin);
         Debug.DrawRay(ray.origin, transform.forward * rayLength, Color.red);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, rayLength))
        {
            if(hit.collider.tag == "Water")
                {
                    print("Water");
                    if(Input.GetMouseButtonDown(1))
                    {
                        water = hit.collider.gameObject;
                        isLaying = true;
                        print("Going to " + water.transform.name);
                    }
                }
            
        }
    }

    public void LayEggs()
    {
       
        Instantiate(eggs,this.transform.position + offset,new Quaternion(-90f,0,0,0));
        
    }

    IEnumerator Laying()
    {
        if(isDelaying)
            yield break;

        isDelaying = true;

        yield return new WaitForSeconds(3);
        this.GetComponent<Movement>().canMove = true;
        LayEggs();
        //this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        print("Eggs Laid");
        isLaying = false;
        isDelaying = false;
        //LayEggs(); 
    }
}
