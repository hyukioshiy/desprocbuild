﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Movement : MonoBehaviour
{
    public Rigidbody rb;
    public float mSpeed;
    public float maxSpeed = 5f;
    public float accSpeed = 50f;
    public float vertSpeed = 2f;
    public float horSpeed = 2f;

    private float yaw = 0f;
    private float pitch = 0f;
    public float angle;

    public GameObject mouthPos;

    public Image bloodBank;
    float bloodAmount;
    public bool canDrink;

    public bool testBool;
    public bool canMove;

    Vector3 offset;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        
    }

    // Start is called before the first frame update
    void Start()
    {
        bloodBank.GetComponent<Image>().fillAmount = 0;
        testBool = false;
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(canMove == true)
        {
            UserInput();
            Rotation();
        }
        
        BiteFunction();


        //rb.velocity.magnitude = Vector3.ClampMagnitude(5f);
        this.transform.eulerAngles = new Vector3(pitch, yaw, 0f);
        //Debug.Log(rb.velocity.magnitude);


        if(Input.GetMouseButtonDown(0))
        {
            if (canDrink == true && testBool == false)
            {
                testBool = true;
                canMove = false;
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;

            }
             
            print("Click");
        }


        if(testBool == true && canMove == false)
        {
            this.transform.position = this.transform.parent.transform.position + offset;
        }
        if(Input.GetMouseButtonUp(0))
        {
            if (canDrink == true || testBool == true)
            {
                testBool = false;
                canMove = true;
                transform.parent = null;
            }
           
        }

        if(testBool == true)
        {
            bloodAmount += (1 * Time.deltaTime) / 20;
            bloodAmount = Mathf.Clamp(bloodAmount, 0, 1);
            bloodBank.GetComponent<Image>().fillAmount = bloodAmount;
        }
        //print(canDrink);

    }

    void UserInput()
    {
        /*
        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            mSpeed = 10f;
            maxSpeed = accSpeed;
        }
        else
        {
            mSpeed = 1f;
            maxSpeed = 8f;
        } */
        if(Input.GetKey(KeyCode.W))
        {
            if(rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
            else
            {
                rb.velocity += transform.forward * mSpeed;// Debug.Log("Pressing");
            }
            //rb.velocity
        }
        if(Input.GetKey(KeyCode.A))
        {
            if(rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
            else
            {
                rb.velocity += transform.right * -mSpeed;// Debug.Log("Pressing");
            }
            //rb.velocity
        }
        if(Input.GetKey(KeyCode.D))
        {
            if(rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
            else
            {
                rb.velocity += transform.right * mSpeed;// Debug.Log("Pressing");
            }
            //rb.velocity
        }
        rb.velocity *= 0.99f;
    }

    void Rotation()
    {
        yaw += horSpeed * Input.GetAxis("Mouse X");
        pitch -= vertSpeed * Input.GetAxis("Mouse Y");
        
        //yaw = Mathf.Clamp(yaw, -360f, 360f);
       // pitch = Mathf.Clamp(pitch, -360f, 360f);
    }

    void BiteFunction()
    {
        Vector3 mouthPosition = mouthPos.transform.position;


        Vector3 mouthDirection = transform.TransformDirection(Vector3.forward);
        RaycastHit hit;

        if (Physics.Raycast(mouthPosition, Quaternion.AngleAxis(139, transform.right) * transform.up, out hit, .12f))
        {
            if (hit.collider.gameObject != null)
            {
                print(hit.collider.gameObject.tag);
                if (hit.collider.gameObject.CompareTag("Wall"))
                {
                    if(canDrink == false)
                    {
                        canDrink = true;
                        this.transform.SetParent(hit.collider.gameObject.transform);
                        offset = this.transform.position - hit.collider.gameObject.transform.position;
                    }
                    
                }
                if(hit.collider.gameObject.tag != "Wall")
                {
                    if (canDrink == true)
                    {
                        canDrink = false;
                    }
                }
            }
            if(hit.collider.gameObject == null)
            {
                if (canDrink == true)
                {
                    canDrink = false;
                }
            }

           
            
            
        }

        else
        {
            if (canDrink == true)
            {
                canDrink = false;
            }
        }
        Debug.DrawRay(mouthPosition, (Quaternion.AngleAxis(139, transform.right) * transform.up) * .12f, Color.green);
    }
    
}
