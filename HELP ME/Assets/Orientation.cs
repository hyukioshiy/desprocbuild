﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orientation : MonoBehaviour
{
    public Animator animator;

    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = player.transform.position;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, player.transform.rotation, .05f * Time.deltaTime * 100);

        if(player.GetComponent<Raycast>().isLaying)
        {
            animator.SetBool("isLaying",true);
            animator.SetBool("isFlying",false);
        }
        else
        {
            animator.SetBool("isLaying",false);
            animator.SetBool("isFlying",true);
        }
    }

}
